# README #

# Installation #

I'm using conda environments:

* conda create -n stanford_dogs python=3.4
* source activate stanford_dogs

To install the dependencies, see requirements/requirements.txt.  However, I used a combination of pip and conda (I was having some problems with pip and ipynb), so you might find it easier to do the following:

* pip install tensorflow-gpu
* pip install keras
* conda install nb_conda
* conda install -c conda-forge matplotlib=2.0.0
* conda install scikit-image
* conda install h5py

# Supplementary Files #

Download the following from the [Stanford Dogs Dataset website](http://vision.stanford.edu/aditya86/ImageNetDogs/):

* Images
* Annotations
* Lists

And place them in a folder that I will refer to as root_dir

# Training #

First configure train.py:

* USER PATHS: set root_dir to the folder containing Images, Annotations and Lists
* TRAIN PARAMS: you can read the inline comments for description of the parameters
* OTHER PATHS: check that these make sense for you.  You will need to create an 'Experiments' folder in the root_dir for saving checkpoints. 

Then do: python train.py

# Testing #

The trained networks can be run on test images using the jupyter notebook: 'Performance_on_test_set.ipynb'