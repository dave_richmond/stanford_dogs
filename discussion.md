# DISCUSSION #

I will describe the justification for my overall approach, followed by a quick discussion of the methodological choices, and finally some results.

# Overall Approach #

I haven't worked on such a data set before, so I had a quick look at this [paper](http://link.springer.com/article/10.1007/s11633-017-1053-3). They propose the following methods:

* Standard ConvNet
* Multiple ConvNets: ensemble, tree-based, ...
* Parts-based Models
* Visual attention Models

As a first step, I wanted to establish a strong baseline, and so I chose to use a standard ConvNet.
An obvious next step would be to train multiple ConvNets.
Parts-based and visual attention models are a different, but attractive approach.
Unfortunately, standard parts-based models require additional annotations.
Visual attention models get around this problem, but so far show only modest improvements over standard ConvNets on fine-grained recognition (as per the paper, above).
I would still be interested to train a [Spatial Transformer Network](https://arxiv.org/pdf/1506.02025.pdf) with multiple learned transformations, as a form of visual attention.

But for now, I will just be using a standard ConvNet.

# Model Architecture #

I started with a VGG-style ConvNet, because this has worked well for me in the past.  

* **Kernels:** All kernels are 3x3, with 1 px padding to maintain the image size.  Convolutions are in pairs or triplets, between each max pool.
* **BatchNorm:** Applied after all convolutions, to speed up training.
* **Activations:** My first attempt at training with ReLU flat-lined.  I replaced this with leaky ReLU and it seemed to fix the problem.
* **Pooling:** Both networks have 5 pooling layers (all 2x2), to shrink the input from a 256x256 image, down to 8x8 before the fully connected layers.
* **Optimizer:** I use Adam, which is a great all-purpose optimizer (I had only one shot at overnight training)
* **Dropout:** To avoid over-fitting.  There are only 12k training images.

As a simple first exploration, I trained two models: 'small' and 'big'.
Big has more convolutions, and more features per convolution.  
Big also has 2 fc layers, before the output layer, whereas small has only 1.
For more information, see code/keras\_models.py

# Data #

## Train/Val/Test split ##

I followed the train/test split of the original paper (by reading in the train\_list.mat file), for ease of comparison.
I then randomly selected 90% of these images for training, and used the remaining images for validation.
This results in an average of 90 training images for each class.
For these conditions, the [authors](http://vision.stanford.edu/aditya86/ImageNetDogs/) report a baseline result of 21% mean accuracy over all classes.

## Pre-processing ##

I applied very simple pre-processing.  Initially, I calculated a mean image and subtracted it.  Here are the mean images for R,G,B separately:

![Mean images](results/mean_image.png)

At some point, I removed this pre-processing step and the model still trained fine, so I left it out.
In the end, the only pre-processing that I did was to resize each image to 256x256, and rescale to [0,1] range.
I looked at the resizing, to make sure it wasn't going to be too problematic.  Here is a quick example.

![Image of resizing](results/image_resizing.png)

## Augmentation ##

The authors describe this data set as challenging based on the large intra-class variation, from pose, age, color, etc.
To try and mitigate the pose variation, I used augmentation.
Keras has a nice data generator, that applies real-time augmentation for a few different classes of transformations.
I applied translation, zoom and horizontal flip.

One thing I was hoping to try, but didn't find time for yet, is to apply smarter augmentation based on the bounding box information stored in the annotation files.
My scripts currently read this information in, but don't use it.
A simple idea is to crop each image randomly, but without removing any of the bounding box.
This would allow for more aggressive augmentation of the training set, because many of the dogs take up only a small region in the image. 

# Training #

I considered starting from a pre-trained VGG-net; and then replacing the output layer with 120-class softmax, and retraining only the final fully connected layer of weights.
However, this didn't give as much flexibility to muck about, and I wasn't too concerned with the absolute performance, so I decided to train from scratch.
I was also a little concerned about the train/test split, since these images come from ImageNet, so there's a chance that some of the test images would have already been seen during training of the VGG model.

I trained both models (small and big) in parallel for approximately 12hrs.
I used a rather conservative base learning rate (1e-4) to avoid possible problems.
This and other training parameters can be found in train.py.

### Training the small model ###

![Training small model: loss](results/small_model_training_curves/loss.png)
![Training small model: acc](results/small_model_training_curves/acc.png)
![Training small model: val loss](results/small_model_training_curves/val_loss.png)
![Training small model: val acc](results/small_model_training_curves/val_acc.png)

### Training the big model ###

![Training big model: loss](results/big_model_training_curves/loss.png)
![Training big model: acc](results/big_model_training_curves/acc.png)
![Training big model: val loss](results/big_model_training_curves/val_loss.png)
![Training big model: val acc](results/big_model_training_curves/val_acc.png)

A few notes on training:

* The big model only covered about half as many epochs (200 vs. 400) as the small model, but actually reached a higher accuracy on the validation set.
* Both models were still improving wrt validation accuracy, but starting to level out.
* Both models were apparently over-fitting the data, based on the loss which was increasing after 50 epochs of training.  However, val accuracy wasn't adversely affected yet.

# Performance on Test Set #

Performance on the test set seemed to mirror the validation set very closely.

* **Top performance on small model:** The best performance averaged over all classes was 41.5%, achieved after 400 epochs.
* **Top performance on big model:** The best performance averaged over all classes was 47.5%, achieved after 200 epochs.

These results are significantly higher than the baseline reported [here](http://vision.stanford.edu/aditya86/ImageNetDogs/).
This could possibly be due to better augmentation, but to be completely honest, my next step would be to search for a bug.

One way to further improve the performance on test set is averaging multiple augmented input images (as in the AlexNet paper).  I didn't explore this.

# Next Steps #

One thing that I was hoping to try, was a multi-task network architecture.
The idea would be to predict both the class, as well as the bounding box coordinates, to make full use of the annotations.
Unfortunately, I ran out of time, but I think this would be relatively straight-forward to implement.
It should give additional supervisory signal to the net, and I wonder how much it would improve the performance.

I'm also curious how the performance of my trained models compares to transferring weights from VGG-net pre-trained on the full ImageNet.