from keras.utils.np_utils import to_categorical
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.preprocessing.image import ImageDataGenerator

import os

from keras_models import small_net, big_net 
from np_utils import load_data, split_train_val, preprocess_images


# ========= USER PATHS ===========

root_dir = '/ssd/Stanford_Dogs'


# ========= TRAIN PARAMS ===========

trial_id = 'trial_1'	# keep your experiments separate
gpuID = 0 				# make this gpu visible, for training multiple models in parallel
net_size = 'small' 		# either 'small' or 'big'.  see keras_models.py for net definition
base_lr = 1e-4 			# parameter for Adam optimizer
batch_size = 16 		# used for both train and val
num_epochs = 500 		# how long to train, in epochs
cp_period = 50 			# how often to save model checkpoints
train_frac = 0.9		# fraction of training data to use during training.  the rest is used for val.

# ========= OTHER PATHS (YOU SHOULDN'T NEED TO SET THESE) ===========

imgs_root_dir = os.path.join(root_dir,'Images')
annotations_root_dir = os.path.join(root_dir,'Annotation')
expts_root_dir = os.path.join(root_dir,'Experiments')

train_list_path = os.path.join(root_dir,'train_list.mat')
# mean_img_path = os.path.join(root_dir,'mean_image.jpg')

checkpoint_path = os.path.join(expts_root_dir,trial_id,'weights-{epoch:02d}.hdf5')
log_path = os.path.join(expts_root_dir,trial_id,'logs')

# ============= MAIN

def run_train():

	# load data
	print('loading data...')
	imgs, labels, BBs = load_data(train_list_path,imgs_root_dir,annotations_root_dir)
	print('finished loading data...')

	# preprocess images and labels
	# imgs = preprocess_images(imgs,mean_img_path)
	labels -= 1
	categorical_labels = to_categorical(labels, num_classes=120)

	# split train val
	train_imgs, train_labels, val_imgs, val_labels = split_train_val(imgs,categorical_labels,train_frac)

	# set visible GPU
	os.environ["CUDA_VISIBLE_DEVICES"]=str(gpuID)
	gpuString = 'gpu:0'

	# callbacks
	checkpoint = ModelCheckpoint(checkpoint_path,period=cp_period)
	tensorboard_log = TensorBoard(log_dir=log_path)
	callbacks_list = [checkpoint, tensorboard_log]

	# set up data generators for real-time augmentation
	train_datagen = ImageDataGenerator(
		width_shift_range=0.1,
		height_shift_range=0.1,
		zoom_range=0.1,
		horizontal_flip=True)

	val_datagen = ImageDataGenerator()

	train_generator = train_datagen.flow(train_imgs,train_labels,batch_size=batch_size,shuffle=True)
	val_generator = val_datagen.flow(val_imgs,val_labels,batch_size=batch_size,shuffle=True)

	# intialize net
	if net_size == 'small':
		net = small_net(base_lr)
	elif net_size == 'big':
		net = big_net(base_lr)

	# train
	num_train_steps = train_imgs.shape[0] // batch_size
	num_val_steps = val_imgs.shape[0] // batch_size

	net.fit_generator(
		train_generator,
		steps_per_epoch=num_train_steps,
		epochs=num_epochs,
		callbacks=callbacks_list,
		validation_data=val_generator,
		validation_steps=num_val_steps)


def main():

	# train net
	run_train()


if __name__ == '__main__':
	main()