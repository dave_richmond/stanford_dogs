import numpy as np
import scipy.io as sio
import skimage.io as skio
import skimage.transform
import re
import os


def read_annotation(fname):

	f = open(fname, 'r', newline=None)
	for line in f:

		find_xmin = re.findall('<xmin>([0-9]+)</xmin>', line)
		find_xmax = re.findall('<xmax>([0-9]+)</xmax>', line)
		find_ymin = re.findall('<ymin>([0-9]+)</ymin>', line)
		find_ymax = re.findall('<ymax>([0-9]+)</ymax>', line)

		if find_xmin:
			xmin = int(find_xmin[0])
		if find_xmax:
			xmax = int(find_xmax[0])
		if find_ymin:
			ymin = int(find_ymin[0])
		if find_ymax:
			ymax = int(find_ymax[0])

	return (xmin,xmax,ymin,ymax)


def load_data(train_list_path,imgs_root_dir,annotations_root_dir,max_num_imgs=-1):

	train_list = sio.loadmat(train_list_path)
	file_list = train_list['file_list']
	annotation_list = train_list['annotation_list']
	label_list = train_list['labels']

	imgs,labels,BBs = [],[],[]

	for idx,_ in enumerate(file_list):

		# images
		img_file = os.path.join(imgs_root_dir,file_list[idx][0][0])
		img = skio.imread(img_file)
		if img.shape[2] != 3:
			print('skipping image: ' + img_file + ' because not in RGB colorspace')
			continue
		img = skimage.transform.resize(img,(256,256))
		imgs.append(img)

		# labels
		labels.append(label_list[idx][0])

		# annotations
		annotation_file = os.path.join(annotations_root_dir, annotation_list[idx][0][0])
		BB = read_annotation(annotation_file)
		BBs.append(BB)

		if idx == (max_num_imgs-1):
			break

	imgs = np.stack(imgs)
	labels = np.stack(labels)
	BBs = np.stack(BBs)

	return imgs, labels, BBs


def split_train_val(imgs,labels,train_frac):

	num_train_samples = int(train_frac * imgs.shape[0])

	all_idx = range(imgs.shape[0])
	train_idx = np.random.choice(all_idx,size=num_train_samples,replace=False)
	train_imgs = imgs[train_idx]
	train_labels = labels[train_idx]

	val_idx = [x for x in all_idx if not x in train_idx]
	val_imgs = imgs[val_idx]
	val_labels = labels[val_idx]

	return train_imgs, train_labels, val_imgs, val_labels


def preprocess_images(imgs,mean_img_path):

	mean_img = np.float64(skimage.io.imread(mean_img_path))
	mean_img = mean_img / 256

	return imgs - mean_img