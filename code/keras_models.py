from keras.layers import Input, Conv2D, Activation, LeakyReLU, MaxPooling2D, Dropout, Flatten, BatchNormalization, Dense
from keras.models import Model
from keras.optimizers import Adam


# ========== COMMON LAYER DEFS ===========

def conv(x,nfeats):
	return Conv2D(nfeats,kernel_size=(3,3),strides=(1,1),padding='same')(x)


def bn(x):
	return BatchNormalization(axis=-1)(x)


def lrelu(x):
	return LeakyReLU(0.2)(x)


def conv_bn_lrelu(x,nfeats):
	return lrelu( bn( conv(x,nfeats) ) )


def pool(x):
	return MaxPooling2D(pool_size=(2,2))(x)


def fc_lrelu_drop(x,nfeats,flatten=True):
	if flatten:
		x = Flatten()(x)
	x = Dense(nfeats)(x)
	x = LeakyReLU(0.2)(x)
	x = Dropout(0.5)(x)
	return x


# ========== NET DEFS ===========

def small_net(base_lr, shape=(256,256,3)):

	# input
	input_layer = Input(shape=shape)

	# first set of convs
	x = conv_bn_lrelu(input_layer,16)
	x = conv_bn_lrelu(x,16)
	x = pool(x)

	# second set of convs
	x = conv_bn_lrelu(x,32)
	x = conv_bn_lrelu(x,32)
	x = pool(x)

	# third set of convs
	x = conv_bn_lrelu(x,64)
	x = conv_bn_lrelu(x,64)
	x = pool(x)

	# fourth set of convs
	x = conv_bn_lrelu(x,128)
	x = conv_bn_lrelu(x,128)
	x = pool(x)

	# fifth set of convs
	x = conv_bn_lrelu(x,256)
	x = conv_bn_lrelu(x,256)
	x = pool(x)

	# fc
	x = fc_lrelu_drop(x,512)

	# output
	pred = Dense(120, activation='softmax')(x)

	net = Model(inputs=input_layer, outputs=pred)
	adam = Adam(lr=base_lr)
	net.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

	return net


def big_net(base_lr, shape=(256,256,3)):

	# input
	input_layer = Input(shape=shape)

	# first set of convs
	x = conv_bn_lrelu(input_layer,32)
	x = conv_bn_lrelu(x,32)
	x = pool(x)

	# second set of convs
	x = conv_bn_lrelu(x,64)
	x = conv_bn_lrelu(x,64)
	x = pool(x)

	# third set of convs
	x = conv_bn_lrelu(x,128)
	x = conv_bn_lrelu(x,128)
	x = conv_bn_lrelu(x,128)
	x = pool(x)

	# fourth set of convs
	x = conv_bn_lrelu(x,256)
	x = conv_bn_lrelu(x,256)
	x = conv_bn_lrelu(x,256)
	x = pool(x)

	# fifth set of convs
	x = conv_bn_lrelu(x,512)
	x = conv_bn_lrelu(x,512)
	x = conv_bn_lrelu(x,512)
	x = pool(x)

	# fc
	x = fc_lrelu_drop(x,1024)
	x = fc_lrelu_drop(x,1024,flatten=False)

	# output
	pred = Dense(120, activation='softmax')(x)

	net = Model(inputs=input_layer, outputs=pred)
	adam = Adam(lr=base_lr)
	net.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

	return net